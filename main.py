from app import create_app
from config import config

# seleccionamos el modo en el que ejecutaremos la aplicacion
enviroment = config['development']
app = create_app(enviroment)

if __name__ == '__main__':
    app.run()
