class Config:
    pass

# clase de desarrollo, almacena todas las configuraciones para la aplicacion
class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://siri:310397@localhost/apiflask'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class TestConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql://siri:310397@localhost/apiflask_test'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

# diccionario para configurar el servidor dependiendo el modo que lo queramos
config = {
    'development': DevelopmentConfig,
    'test': TestConfig
}